import { ethers } from "hardhat";
import { utils, BigNumber } from "ethers";

async function main() {
  const [owner, ...addrs] = await ethers.getSigners();
  const defaultMinimumQuorum = 1; // 1%
  // 48 hours
  const defaultDebatinPeriodDurationSeconds = 60 * 60 * 24 * 2;

  const initialTokenBalance: BigNumber = utils.parseUnits("100000000", 18);
  const TestContract = await ethers.getContractFactory("Test");
  const testContract = await TestContract.deploy();

  const Token = await ethers.getContractFactory("VoteToken");
  const voteToken = await Token.deploy(
    "VoteToken",
    "VTN",
    initialTokenBalance
  );

  const DAOContract = await ethers.getContractFactory("DAO");
  const dao = await DAOContract.deploy(
    owner.address,
    voteToken.address,
    defaultMinimumQuorum,
    defaultDebatinPeriodDurationSeconds,
  );
  await testContract.setDao(dao.address);
  console.log("VoteToken address: ", voteToken.address);
  console.log("DAOContract address: ", dao.address);
  console.log("TestContract address: ", testContract.address);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
