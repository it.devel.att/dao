const localPrivateKey = process.env.LOCAL_PRIVATE_KEY || "0xdf57089febbacf7ba0bc227dafbffa9fc08a93fdc68e1e42411a14efcf23656e";

const rinkebyURL = process.env.RINKEBY_URL || "https://rinkeby.infura.io/v3/secret_hash";
const rinkebyPrivateKey = process.env.RINKEBY_PRIVATE_KEY || "0xdf57089febbacf7ba0bc227dafbffa9fc08a93fdc68e1e42411a14efcf23656e";


export const secretConfig = {
    localPrivateKey: localPrivateKey,
    rinkeby: {
        url: rinkebyURL,
        privateKey: rinkebyPrivateKey,
    }
}